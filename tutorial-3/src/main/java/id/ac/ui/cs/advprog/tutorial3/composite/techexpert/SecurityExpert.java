package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class SecurityExpert extends Employees{

	public SecurityExpert(String name, double salary) {
		if(salary < 70000.00){
			throw new IllegalArgumentException("Salary Security Expert tidak boleh dibawah 70.000.00");
		}
		this.name = name;
		this.salary = salary;
		this.role = "Security Expert";
	}

	@Override
	public double getSalary() {
		// TODO Auto-generated method stub
		return salary;
	}

}
