package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Cto extends Employees{
    public Cto(String name, double salary) {
    	//TODO Implement
    	if(salary < 100000.00){
			throw new IllegalArgumentException("Salary CTO tidak boleh dibawah Rp 100.000.00");
		}
		
		this.name = name;
		this.salary = salary;
		this.role = "CTO";
	}
	
	@Override
	public double getSalary() {
	    //TODO Implement
		return salary;
	}
}
