package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class NetworkExpert extends Employees{

	public NetworkExpert(String name, double salary) {
		if(salary < 50000.00){
			throw new IllegalArgumentException("Salary Network Expert tidak boleh dibawah 50.000.00");
		}
		this.name = name;
		this.salary = salary;
		this.role = "Network Expert";
	}

	@Override
	public double getSalary() {
		// TODO Auto-generated method stub
		return salary;
	}
	
}
