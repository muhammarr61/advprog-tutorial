package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class UiUxDesigner extends Employees{

	public UiUxDesigner(String name, double salary) {
		if(salary < 90000.00){
			throw new IllegalArgumentException("Salary UI/UX Designer tidak boleh dibawah 90.000.00");
		}
		this.name = name;
		this.salary = salary;
		this.role = "UI/UX Designer";
	}

	@Override
	public double getSalary() {
		// TODO Auto-generated method stub
		return salary;
	}

}
