package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class BackendProgrammer extends Employees {
	
	public BackendProgrammer(String name, double salary) {
		// TODO Auto-generated constructor stub
		if(salary < 20000.00){
			throw new IllegalArgumentException("Salary Back End Programmer tidak boleh dibawah 20.000.00");
		}
		this.name = name;
		this.salary = salary;
		this.role = "Back End Programmer";
	}

	@Override
	public double getSalary() {
		// TODO Auto-generated method stub
		return salary;
	}
    //TODO Implement
	
}
