package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class MozzarellaSlice implements Cheese{
	public String toString() {
        return "Mozzarella Slices";
    }
}
