package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class ThickThickCrustDough implements Dough {
    public String toString() {
        return "Extra Extra Thick Crust Dough";
    }
}
