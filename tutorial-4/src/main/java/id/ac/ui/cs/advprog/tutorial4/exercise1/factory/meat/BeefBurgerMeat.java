package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.meat;

public class BeefBurgerMeat implements Meats {

    public String toString() {
        return "Classic American Beef Burger Meat";
    }
}
