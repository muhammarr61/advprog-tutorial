package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.meat;

public interface Meats {
    public String toString();
}
