package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory;

public class AlohaPizza extends Pizza{
	
	PizzaIngredientFactory ingredientFactory;

    public AlohaPizza(PizzaIngredientFactory ingredientFactory) {
        this.ingredientFactory = ingredientFactory;
    }

    public void prepare() {
        System.out.println("Preparing " + name);
        dough = ingredientFactory.createDough();
        sauce = ingredientFactory.createSauce();
        clam = ingredientFactory.createClam();
        veggies = ingredientFactory.createVeggies();
    }
}
