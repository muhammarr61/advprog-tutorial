package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.AlohaPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheeseBurgerPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;

public class DepokPizzaStore extends PizzaStore {

    @Override
    protected Pizza createPizza(String item) {
        Pizza pizza = null;
        PizzaIngredientFactory ingredientFactory =
                new DepokPizzaIngredientFactory();

        if (item.equals("aloha")) {

            pizza = new AlohaPizza(ingredientFactory);
            pizza.setName("Depok Style Aloha Pizza");

        } else if (item.equals("burger")) {

            pizza = new CheeseBurgerPizza(ingredientFactory);
            pizza.setName("Depok Style Classic American Cheese Burger Pizza");

        } 
        
        return pizza;
    }

}
