package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class ThinThinCrustDough implements Dough {
    public String toString() {
        return "Extra Thin Crust Dough";
    }
}
