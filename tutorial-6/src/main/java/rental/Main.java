package rental;

public class Main {
    public static void main(String arg[]) {
        Customer customer = new Customer("Alice");
        Movie movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        Rental rent = new Rental(movie, 3);
        customer.addRental(rent);
        movie = new Movie("SAW IV", Movie.CHILDREN);
        rent = new Rental(movie, 7);
        customer.addRental(rent);
        movie = new Movie("The Avengers: Infinity War", Movie.NEW_RELEASE);
        rent = new Rental(movie, 1);
        customer.addRental(rent);
        System.out.println(customer.statement());
    }
}
