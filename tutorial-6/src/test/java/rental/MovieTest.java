package rental;

import org.junit.Test;

import static org.junit.Assert.*;

public class MovieTest {
    private Movie movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable

    @Test
    public void getTitle() {
        setup();
        assertEquals("Who Killed Captain Alex?", movie.getTitle());
    }

    @Test
    public void setTitle() {
        setup();
        movie.setTitle("Bad Black");

        assertEquals("Bad Black", movie.getTitle());
    }

    @Test
    public void getPriceCode() {
        setup();
        assertEquals(Movie.REGULAR, movie.getPriceCode());
    }

    @Test
    public void setPriceCode() {
        setup();
        movie.setPriceCode(Movie.CHILDREN);

        assertEquals(Movie.CHILDREN, movie.getPriceCode());
    }

    private void setup() {
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
    }
}