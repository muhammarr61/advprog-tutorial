package rental;

import org.junit.Test;

import static org.junit.Assert.*;

public class RentalTest {
    private Movie movie;
    private Rental rent;

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable

    @Test
    public void getMovie() {
        setup();
        assertEquals(movie, rent.getMovie());
    }

    @Test
    public void getDaysRented() {
        setup();
        assertEquals(3, rent.getDaysRented());
    }

    private void setup() {
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        rent = new Rental(movie, 3);
    }
}