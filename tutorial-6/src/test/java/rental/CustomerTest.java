package rental;

import org.junit.Test;

import static org.junit.Assert.*;

public class CustomerTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable

    @Test
    public void statementWithSingleMovie() {
        Movie movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        Rental rent = new Rental(movie, 3);
        Customer customer = new Customer("Alice");
        assertEquals("Alice", customer.getName());
        customer.addRental(rent);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    @Test
    public void statementWithMultipleMovies() {
        // TODO Implement me!
        Customer customer = new Customer("Alice");
        Movie movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        Rental rent = new Rental(movie, 3);
        customer.addRental(rent);
        movie = new Movie("SAW IV", Movie.CHILDREN);
        rent = new Rental(movie, 7);
        customer.addRental(rent);
        movie = new Movie("The Avengers: Infinity War", Movie.NEW_RELEASE);
        rent = new Rental(movie, 1);
        customer.addRental(rent);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(6, lines.length);
        assertTrue(result.contains("Amount owed is 14.0"));
        assertTrue(result.contains("You earned 3 frequent renter points"));
    }
}