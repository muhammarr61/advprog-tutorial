import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ScoreGroupingTest {
    // TODO Implement me!
    // Increase code coverage in ScoreGrouping class
    // by creating unit test(s)!
    private Map<String, Integer> scores;
    private Map<Integer, List<String>> expectedResult;

    @Before
    public void setUp() throws Exception {
        scores = new HashMap<>();

        scores.put("Alice", 12);
        scores.put("Bob", 15);
        scores.put("Charlie", 11);
        scores.put("Delta", 15);
        scores.put("Emi", 15);
        scores.put("Foxtrot", 11);

        expectedResult = new HashMap<>();

        expectedResult.put(11, Arrays.asList("Charlie", "Foxtrot"));
        expectedResult.put(12, Arrays.asList("Alice"));
        expectedResult.put(15, Arrays.asList("Emi", "Bob", "Delta"));
    }

    @Test
    public void testScores() throws Exception {
        scores.forEach( (name, score)  -> assertEquals( expectedResult.get(score), ScoreGrouping.groupByScores(scores).get(score)));
    }
}