package applicant;

import org.junit.Before;
import org.junit.Test;

import java.util.function.Predicate;

import static applicant.Applicant.evaluate;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ApplicantTest {
    // TODO Implement me!
    // Increase code coverage in Applicant class
    // by creating unit test(s)!

    Applicant applicant;
    Predicate<Applicant> creditCheck;
    Predicate<Applicant> employmentCheck;
    Predicate<Applicant> crimeCheck;

    @Before
    public void setUp() throws Exception {
        applicant = new Applicant();

        creditCheck = theApplicant -> theApplicant.getCreditScore() > 600;
        employmentCheck = theApplicant -> theApplicant.getEmploymentYears() > 0;
        crimeCheck = theApplicant -> !theApplicant.hasCriminalRecord();
    }

    @Test
    public void testApplicant() throws Exception {
        assertTrue(evaluate(applicant, creditCheck));
        assertTrue(evaluate(applicant, creditCheck.and(employmentCheck)));
        assertFalse(evaluate(applicant, crimeCheck.and(employmentCheck)));
        assertFalse(evaluate(applicant, crimeCheck.and(creditCheck).and(employmentCheck)));
    }
}
