package sorting;


public class Sorter {
    /**
     * Some sorting algorithm that possibly the slowest algorithm.
     *
     * @param inputArr of integer that need to be sorted.
     * @return a sorted inputArr of integer.
     */
    private static int[] inputArr;
    private static int[] tempMergArr;
    private static int length;

    public static int[] slowSort(int[] inputArr) {
        int temp;
        for (int i = 1; i < inputArr.length; i++) {
            for (int j = i; j > 0; j--) {
                if (inputArr[j] < inputArr[j - 1]) {
                    temp = inputArr[j];
                    inputArr[j] = inputArr[j - 1];
                    inputArr[j - 1] = temp;
                }
            }
        }
        return inputArr;
    }

    public static int[] BubbleSort(int inputArr[]) {
        int n = inputArr.length;
        int k;
        for (int m = n; m >= 0; m--) {
            for (int i = 0; i < n - 1; i++) {
                k = i + 1;
                if (inputArr[i] > inputArr[k]) {
                    swapNumbers(i, k, inputArr);
                }
            }
        }
        return inputArr;
    }

    public static int[] SelectionSort(int[] inputArr){

        for (int i = 0; i < inputArr.length - 1; i++)
        {
            int index = i;
            for (int j = i + 1; j < inputArr.length; j++)
                if (inputArr[j] < inputArr[index])
                    index = j;

            int smallerNumber = inputArr[index];
            inputArr[index] = inputArr[i];
            inputArr[i] = smallerNumber;
        }
        return inputArr;
    }

    public static int[] InsertionSort(int[] inputArr){

        int temp;
        for (int i = 1; i < inputArr.length; i++) {
            for(int j = i ; j > 0 ; j--){
                if(inputArr[j] < inputArr[j-1]){
                    temp = inputArr[j];
                    inputArr[j] = inputArr[j-1];
                    inputArr[j-1] = temp;
                }
            }
        }
        return inputArr;
    }

    public static int[] QuickSort(int[] inputArr) {

        if (inputArr == null || inputArr.length == 0) {
            return null;
        }
        int length = inputArr.length;
        return QuickSort(inputArr, 0, length - 1);
    }

    private static int[] QuickSort(int[] inputArr, int lowerIndex, int higherIndex) {

        int i = lowerIndex;
        int j = higherIndex;
        // calculate pivot number, I am taking pivot as middle index number
        int pivot = inputArr[lowerIndex+(higherIndex-lowerIndex)/2];
        // Divide into two arrays
        while (i <= j) {
            /**
             * In each iteration, we will identify a number from left side which
             * is greater then the pivot value, and also we will identify a number
             * from right side which is less then the pivot value. Once the search
             * is done, then we exchange both numbers.
             */
            while (inputArr[i] < pivot) {
                i++;
            }
            while (inputArr[j] > pivot) {
                j--;
            }
            if (i <= j) {
                exchangeNumbers(inputArr, i, j);
                //move index to next position on both sides
                i++;
                j--;
            }
        }
        // call quickSort() method recursively
        if (lowerIndex < j)
            QuickSort(inputArr, lowerIndex, j);
        if (i < higherIndex)
            QuickSort(inputArr, i, higherIndex);
        return inputArr;
    }
    public static int[] MergeSort(int inputArr[]) {
        Sorter.inputArr = inputArr;
        Sorter.length = inputArr.length;
        Sorter.tempMergArr = new int[length];
        MergeSort(0, length - 1);
        return Sorter.inputArr;
    }

    private static void MergeSort(int lowerIndex, int higherIndex) {

        if (lowerIndex < higherIndex) {
            int middle = lowerIndex + (higherIndex - lowerIndex) / 2;
            // Below step sorts the left side of the inputArr
            MergeSort(lowerIndex, middle);
            // Below step sorts the right side of the inputArr
            MergeSort(middle + 1, higherIndex);
            // Now merge both sides
            mergeParts(lowerIndex, middle, higherIndex);
        }
    }

    private static void mergeParts(int lowerIndex, int middle, int higherIndex) {

        for (int i = lowerIndex; i <= higherIndex; i++) {
            tempMergArr[i] = inputArr[i];
        }
        int i = lowerIndex;
        int j = middle + 1;
        int k = lowerIndex;
        while (i <= middle && j <= higherIndex) {
            if (tempMergArr[i] <= tempMergArr[j]) {
                inputArr[k] = tempMergArr[i];
                i++;
            } else {
                inputArr[k] = tempMergArr[j];
                j++;
            }
            k++;
        }
        while (i <= middle) {
            inputArr[k] = tempMergArr[i];
            k++;
            i++;
        }

    }

    private static void exchangeNumbers(int[] inputArr, int i, int j) {
        int temp = inputArr[i];
        inputArr[i] = inputArr[j];
        inputArr[j] = temp;
    }

    private static void swapNumbers(int i, int j, int[] inputArr) {

        int temp;
        temp = inputArr[i];
        inputArr[i] = inputArr[j];
        inputArr[j] = temp;
    }

}
