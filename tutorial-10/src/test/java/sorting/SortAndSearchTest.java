package sorting;

import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SortAndSearchTest {
    //TODO Implement, apply your test cases here
    private static String pathFile = "src/test/java/sorting/input/sortingProblem.txt";
    private static int numberOfItemToBeSorted = 50000;
    private int[] sequenceInput;
    private int[] sorted = IntStream.rangeClosed(1,50000).toArray();

    @Before
    public void setUp() throws Exception {
        File sortingProblemFile = new File(pathFile);
        FileReader fileReader = new FileReader(sortingProblemFile);
        sequenceInput = new int[numberOfItemToBeSorted];

        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String currentLine;
        int indexOfSequence = 0;
        while ((currentLine = bufferedReader.readLine()) != null) {
            sequenceInput[indexOfSequence] = Integer.parseInt(currentLine);
            indexOfSequence++;
        }
    }
    @Test
    public void testSearchUnSorted() throws Exception {
        assertEquals(49999, Finder.slowSearch(sequenceInput, 40738));
    }

    @Test
    public void testSort() throws Exception {
        int[] sort = Main.sort(sequenceInput);
        for (int i = 0; i < sort.length-1; i++) {
            assertTrue( sort[i] < sort[i + 1] );
        }
    }

    @Test
    public void testSearchSorted() throws Exception {
        assertEquals(40737, Finder.binarySearch(sorted, 40738));
    }
}
